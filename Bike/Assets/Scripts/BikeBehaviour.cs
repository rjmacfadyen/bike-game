﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BikeBehaviour : MonoBehaviour
{
    private float driveInput = 0;
    private float horizontalInput = 0;
    private float verticalInput = 0;
    private bool boostInput = false;

    [SerializeField]
    private GameObject m_com;
    [Header("Parts")]
    private Rigidbody m_rigidbody;
    private Transform m_transform;
    [SerializeField]
    private WheelCollider[] m_wheels = new WheelCollider[2];

    [Header("Control")]
    [SerializeField]
    private float m_torque;
    [SerializeField]
    private float m_reverseTorque;
    [SerializeField]
    private float m_braking;

    [SerializeField]
    private float m_maxForwardRPM = 3000;
    [SerializeField]
    private float m_maxReverseRPM = 1500;

    [SerializeField]
    private float m_boostMultiplier;
    [SerializeField]
    private float m_boostGain;
    private float m_boost = 100;

    [SerializeField]
    private float m_steerLockAngle;
    private float m_targetSteer = 0;
    private float m_targetLean = 0;

    [SerializeField]
    private float m_pitchControl;
    [SerializeField]
    private float m_rollControl;

    private bool m_wasAirborne = false;
    private float m_airtime = 0;

    private float m_flipCount = 0;
    private float m_rollCount = 0;

    private string[] m_stuntMultiples = { "", "", "Double ", "Triple ", "Quad ", "Massive " };

    private bool m_controllable = true;

    private Vector3 m_respawnPos;
    private Quaternion m_respawnRot;

    [Header("Visuals")]
    [SerializeField]
    private ParticleSystem m_booster;
    [SerializeField]
    private GameObject m_steering;
    [SerializeField]
    private float m_steeringAdjustment = 0;

    [Header("GUI")]
    [SerializeField]
    private bool m_useGui;
    [SerializeField]
    private UI m_gui;

    private int m_currentLap = 1;
    private int m_nextCheckpoint = 0;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = GetComponent<Transform>();

        m_respawnPos = m_transform.position;
        m_respawnRot = m_transform.rotation;

        m_wheels[0].brakeTorque = m_wheels[1].brakeTorque = m_braking;
    }

    void FixedUpdate()
    {
        if (m_controllable)
        {
            Drive();
            Boost();
            if (!Airborne())
            {
                Balance();
            }
            Stunts();
        }

        if (m_transform.position.y < -30)
        {
            Respawn();
        }
    }

    public void SendInputs(float drive, float horizontal, float vertical, bool boost)
    {
        driveInput = drive;
        horizontalInput = horizontal;
        verticalInput = vertical;
        boostInput = boost;
    }

    private void Drive()
    {
        // thrustTorque = accel * (m_CurrentTorque / 2f);
        // float input = driveInput;

        Debug.Log(m_wheels[1].rpm);

        m_wheels[0].brakeTorque = m_wheels[1].brakeTorque = 0; // = m_wheels[2].brakeTorque = m_wheels[3].brakeTorque = 0;
        if (driveInput > 0)
        {
            m_wheels[1].motorTorque = m_torque * driveInput * Mathf.Clamp01((m_maxForwardRPM - m_wheels[1].rpm) / m_maxForwardRPM); // m_wheels[2].motorTorque = m_wheels[3].motorTorque = m_torque * input; // accelerate
        }
        else if (driveInput < 0)
        {
            m_wheels[1].motorTorque = m_reverseTorque * driveInput * Mathf.Clamp01((m_maxReverseRPM - Mathf.Abs(m_wheels[1].rpm)) / m_maxReverseRPM); ;  // m_wheels[2].motorTorque = m_wheels[3].motorTorque = m_reverseTorque * input;
        }

        if (m_wheels[1].rpm / driveInput < 0)
        {
            m_wheels[0].brakeTorque = m_wheels[1].brakeTorque = m_braking * Mathf.Abs(driveInput); // m_wheels[0].brakeTorque = m_wheels[1].brakeTorque = m_wheels[2].brakeTorque = m_wheels[3].brakeTorque = m_braking * -input; // brake
            Debug.Log("brake");
        }

        //Debug.Log(IsGrounded(m_wheels[0], 0.05f) + " " + IsGrounded(m_wheels[0], 0.05f));
    }

    private void Boost()
    {
        ParticleSystem.EmissionModule e = m_booster.emission;

        if (m_boost > 0 && boostInput)
        {
            m_boost--;
            m_rigidbody.AddForce(m_transform.forward * m_boostMultiplier);

            e.enabled = true;
        }
        else
        {
            e.enabled = false;
        }

    }

    private void Balance()
    {
        m_targetSteer = Input.GetAxis("Horizontal") * m_steerLockAngle;

        m_wheels[0].steerAngle = m_targetSteer; // = m_wheels[1].steerAngle = m_targetSteer;

        m_steering.transform.localRotation = Quaternion.AngleAxis(m_targetSteer + m_steeringAdjustment, Vector3.up);

        float lean = Loop(m_transform.rotation.eulerAngles.z, -180, 180);
        m_com.transform.localPosition = Quaternion.AngleAxis(-1.5f * lean, Vector3.forward) * Vector3.up;
        m_rigidbody.centerOfMass = m_com.transform.localPosition;
    }

    private void Stunts()
    {
        if (Airborne())
        {
            AirSpin();
            if (!m_wasAirborne)
            {
                m_flipCount = 0;
                m_rollCount = 0;
                m_airtime = Time.time;
            }
        }
        else if (m_wasAirborne && !Airborne())
        {
            m_airtime = Time.time - m_airtime;
            if (m_airtime > 0.5f) // ignore small bumps
            {
                if (true) // (GoodLanding())
                {
                    if (m_useGui)
                    {
                        string desc = StuntDescription();
                        if (StuntDescription() != "!")
                        {
                            m_gui.Flash(StuntDescription() + "\n+" + (int)BoostBonus() + " boost");
                        }
                    }

                    m_boost += BoostBonus();
                }
                else
                {
                    Crash();
                }
            }
            Stabilize();
            //m_rigidbody.centerOfMass = Vector3.down;
        }

        m_wasAirborne = Airborne();
    }

    private void AirSpin()
    {
        m_rigidbody.AddTorque(Input.GetAxis("Vertical") * m_pitchControl, 0, Input.GetAxis("Horizontal") * m_rollControl);

        // add to flip and roll count
        m_flipCount += m_rigidbody.angularVelocity.x / (2 * Mathf.PI) * Time.fixedDeltaTime;
        m_rollCount += m_rigidbody.angularVelocity.z / (2 * Mathf.PI) * Time.fixedDeltaTime;
    }

    private bool Airborne()
    {
        // return !IsGrounded(m_wheels[0], 0.01f) && !IsGrounded(m_wheels[0], 0.01f);
        return !Physics.Raycast(m_rigidbody.position, -m_rigidbody.transform.up, 0.5f);
    }

    private bool GoodLanding()
    {
        return (Mathf.Abs(Loop(m_rigidbody.rotation.eulerAngles.z, -180, 180)) < 30);
    }

    private void Stabilize()
    {
        m_rigidbody.angularVelocity *= 0.05f;
    }

    public void Crash()
    {
        if (m_controllable)
        {
            if (m_useGui)
            {
                m_gui.Flash("Bad Landing!");
            }
            CancelInvoke("Respawn");
            Invoke("Respawn", 4);
            m_controllable = false;

            foreach (Rider r in GetComponentsInChildren<Rider>())
            {
                r.Break();
            }

            ParticleSystem.EmissionModule e = m_booster.emission;
            e.enabled = false;

            //m_rigidbody.constraints = RigidbodyConstraints.None;
        }
    }

    public void Respawn()
    {
        CancelInvoke("Respawn");

        m_transform.position = m_respawnPos;
        m_transform.rotation = m_respawnRot;
        m_rigidbody.ResetInertiaTensor();

        //m_rigidbody.constraints = RigidbodyConstraints.FreezePositionX & RigidbodyConstraints.FreezeRotationY & RigidbodyConstraints.FreezeRotationZ;

        m_rigidbody.velocity = Vector3.zero;
        m_rigidbody.angularVelocity = Vector3.zero;

        foreach (WheelCollider w in m_wheels)
        {
            w.motorTorque = 0;
            w.brakeTorque = Mathf.Infinity;
            w.steerAngle = 0;
        }

        foreach (Rider r in GetComponentsInChildren<Rider>())
        {
            r.Reset();
        }

        m_flipCount = 0;
        m_rollCount = 0;

        Invoke("Reset", Time.fixedDeltaTime * 2);
    }

    private void Reset()
    {
        m_controllable = true;
    }

    private float BoostBonus()
    {
        return ((Mathf.Abs(m_flipCount) + Mathf.Abs(m_rollCount)) * m_boostGain);
    }

    private string StuntDescription()
    {
        List<string> stunts = new List<string>();
        string result = "";

        int flips = (int)Mathf.Round(Mathf.Abs(m_flipCount));
        int rolls = (int)Mathf.Round(Mathf.Abs(m_rollCount));

        if (flips > 0)
        {
            if (flips >= m_stuntMultiples.Length)
            {
                flips = m_stuntMultiples.Length - 1;
            }
            result = m_stuntMultiples[flips];
            if (m_flipCount > 0)
            {
                result += "Frontflip";
            }
            else
            {
                result += "Backflip";
            }
            stunts.Add(result);
        }

        if (rolls > 0)
        {
            if (rolls >= m_stuntMultiples.Length)
            {
                rolls = m_stuntMultiples.Length - 1;
            }
            result = m_stuntMultiples[rolls] + "Roll";

            stunts.Add(result);
        }

        // put together all stunts into one string
        result = "";
        for (int i = 0; i < stunts.Count; i++)
        {
            if (i != 0)
            {
                result += " + ";
            }
            result += stunts[i];
        }

        result += "!";

        return result;
    }

    private bool IsGrounded(WheelCollider wheel, float distance) // helper to detect if wheel is within distance of ground, since the default isGrounded() method is too sensitive
    {
        return Physics.Raycast(wheel.transform.position, -wheel.transform.up, wheel.radius + distance);
    }

    public void setSpawn(Transform t)
    {
        m_respawnPos = t.position;
        m_respawnRot = t.rotation;
    }

    void OnCollisionEnter(Collision col)
    {
        //Debug.Log("Crashed");

        //if (col.gameObject.CompareTag("Ground"))
        //{
        //    Crash();
        //}
    }

    void OnGUI()
    {
        if (m_useGui)
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 30;
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = Color.white;

            GUI.Box(new Rect(0, 0, 500, 200), "Boost: " + Mathf.CeilToInt(m_boost), style);
        }
    }

    private float Loop(float val, float min, float max)
    {
        return (((val + max) % (max - min)) + min);
    }
}