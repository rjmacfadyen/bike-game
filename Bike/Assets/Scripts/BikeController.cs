﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BikeController : MonoBehaviour
{
    private BikeBehaviour behaviour;

    [SerializeField]
    private float respawnTime = 1;
    private float respawnHeld = 0;

    void Start()
    {
        behaviour = gameObject.GetComponent<BikeBehaviour>();
    }

    void Update()
    {
        behaviour.SendInputs(Input.GetAxis("Drive"), Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), Input.GetButton("Boost"));

        if (Input.GetButtonDown("Respawn") || (Input.GetButton("Respawn") && respawnHeld > 0))
        {
            respawnHeld += Time.deltaTime;
            if (respawnHeld > respawnTime)
            {
                behaviour.Respawn();
                respawnHeld = 0;
            }
        }
        else
        {
            respawnHeld = 0;
        }
    }
}