﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {

    [SerializeField]
    private int index;
    private Transform normalTransform;

    void Start()
    {
        normalTransform = new GameObject("normalObject").transform;

        normalTransform.position = transform.position;
        normalTransform.rotation = Quaternion.identity;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bike"))
        {
            gameObject.GetComponentInParent<CourseController>().Checkpoint(index, normalTransform, other.GetComponent<BikeBehaviour>());
        }
    }

    public int Index
    {
        get { return index; }
    }


}
