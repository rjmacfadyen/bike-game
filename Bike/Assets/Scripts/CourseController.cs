﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CourseController : MonoBehaviour {

    private int countDown = 3;
    private float startTime = 0;
    private bool playing = false;
    private bool finished = false;
    private float finishTime = 0;

    [SerializeField]
    private int laps = 1;
    private int currentLap = 1;
    private int nextCheckpoint = 0;
    private int checkpoints = 1;

    [SerializeField]
    private UI gui;

    [SerializeField]
    private BikeController bike;
    [SerializeField]
    private GhostRecorder ghostRecorder;
    [SerializeField]
    private Transform recordBike;
    [SerializeField]
    private GhostController ghostBike;

    // Use this for initialization
    void Start()
    {
        bike.enabled = false;

        ghostBike.Load("test.ghost");
        ghostRecorder.Record();
        ghostBike.Play();
        Invoke("CountDown", 2);
    }

    void CountDown()
    {
        if (countDown > 0)
        {
            gui.Flash(countDown.ToString());
            countDown--;
            Invoke("CountDown", 2);
        }
        else
        {
            gui.Flash("Race!");
            StartRace();
        }
    }

    void StartRace()
    {
        checkpoints = gameObject.GetComponentsInChildren<Checkpoint>().Length;
        //bike = GameObject.FindGameObjectWithTag("Bike").GetComponent<BikeBehaviour>();

        bike.enabled = true;

        startTime = Time.time;
        playing = true;

    }

    public void Checkpoint(int index, Transform pos, BikeBehaviour bike)
    {
        if (index == nextCheckpoint)
        {
            nextCheckpoint = (nextCheckpoint + 1) % checkpoints;

            if (nextCheckpoint == 0)
            {
                currentLap++;
                if (currentLap > laps)
                {
                    Win();
                }
                else
                {
                    gui.Flash("Lap " + currentLap + "/" + laps);
                }
            }
            else
            {
                gui.Flash("Checkpoint!");
            }

            bike.setSpawn(pos);
        }
    }

    private void Win()
    {
        ghostRecorder.Stop();

        finished = true;
        finishTime = Time.time - startTime;

        Debug.Log("Finished");
        gui.Flash("Race won!");
        Invoke("Finish", 3);
    }

    private void Finish()
    {
        if (ghostRecorder.GetTime() < ghostBike.GetTime())
        {
            ghostRecorder.Save("test.ghost");
        }
        gui.ShowEnding(finishTime, ghostRecorder.GetTime() < ghostBike.GetTime());

        foreach (Rigidbody r in bike.GetComponentsInChildren<Rigidbody>())
        {
            r.drag *= 1000000;
            r.angularDrag += 1000000;
        }
    }

    public void Restart()
    {
        // SceneManager.UnloadScene("Prototype");
        SceneManager.LoadScene("Prototype");
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 30;
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.white;
        style.alignment = TextAnchor.UpperRight;

        if (finished)
        {
            GUI.Box(new Rect(Screen.width - 500, 0, 500, 200), System.String.Format("{0:F2}", finishTime), style);
        }
        else if (playing)
        {
            GUI.Box(new Rect(Screen.width - 500, 0, 500, 200), System.String.Format("{0:F2}", Time.time - startTime), style);
        }
        else
        {
            GUI.Box(new Rect(Screen.width - 500, 0, 500, 200), System.String.Format("0.00"), style);
        }
    }
}
