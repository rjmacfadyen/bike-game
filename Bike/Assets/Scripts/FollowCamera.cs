﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

    [SerializeField]
    private GameObject target;

    [SerializeField]
    private float behind;
    [SerializeField]
    private float above;
    [SerializeField]
    private float down;

    private Vector3 offset;

    [SerializeField]
    [Range(0f, 1f)]
    private float smoothing = 0.9f;

	// Use this for initialization
	void Start () {

        offset = new Vector3(0, above, -behind);
        transform.position = target.transform.position + offset;
        transform.rotation = Quaternion.Euler(new Vector3(down, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
        // transform.position = Vector3.Lerp(transform.position, target.transform.position + target.transform.rotation * offset, smoothing);

        Vector3 targetPosition = target.transform.position;

        Vector3 targetVector = target.transform.rotation.eulerAngles;
        targetVector.x = 0; // don't move camera up and down with target pitch

        Quaternion targetRotation = Quaternion.Euler(targetVector);

        transform.position = target.transform.position + targetRotation * offset;

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(
            down, target.transform.rotation.eulerAngles.y, target.transform.rotation.eulerAngles.z)), smoothing);
	}
}
