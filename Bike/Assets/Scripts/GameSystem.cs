﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSystem : MonoBehaviour 
{
	// Use this for initialization
	void Awake () 
    {
        DontDestroyOnLoad(gameObject); //doesn't destroy the game object when loads the next scene
	}

    public void LoadLevel(int _level)
    {
        SceneManager.LoadScene(_level);
    }

    public void LoadLevel(string _level)
    {
        SceneManager.LoadScene(_level);
    }

    public void Enable(GameObject _obj)
    {
        _obj.SetActive(true);
    }

    public void Disable(GameObject _obj)
    {
        _obj.SetActive(false);
    }

}
