﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

public class GhostController : MonoBehaviour {

    private GhostTransform[] transforms = new GhostTransform[0];
    private bool playing = false;
    private float startTime = 0;

    public void Play()
    {
        startTime = Time.time;
        playing = true;
    }

    public void Stop()
    {
        playing = false;
    }

    public void Resume()
    {
        playing = true;
    }

    public void Load(string file)
    {
        transforms = new GhostTransform[0];

        XmlSerializer s = new XmlSerializer(typeof(GhostTransform[]));
        StreamReader r = new StreamReader(file);

        transforms = (GhostTransform[]) s.Deserialize(r);

        r.Close();   
    }	

	void Update ()
    {
        float frame = (Time.time - startTime) / Time.fixedDeltaTime;

        if (playing && frame < transforms.Length)
        {

            if (frame >= 0)
            {
                try
                {
                    transform.position = Vector3.Lerp(transforms[Mathf.FloorToInt(frame)].position, transforms[Mathf.CeilToInt(frame)].position, frame % 1);
                    transform.rotation = Quaternion.Lerp(transforms[Mathf.FloorToInt(frame)].rotation, transforms[Mathf.CeilToInt(frame)].rotation, frame % 1);
                }
                catch (System.IndexOutOfRangeException ex)
                {
                    Debug.Log("Ghost recorder failed to load frame " + frame);
                }
            }

            frame++;
        }
        else
        {
            playing = false;
        }
	}

    public float GetTime()
    {
        if (transforms != null)
        {
            return (transforms.Length * Time.fixedDeltaTime);
        }
        else
        {
            return 0;
        }
    }
}
