﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

public class GhostRecorder : MonoBehaviour {

    private List<GhostTransform> transforms = new List<GhostTransform>();
    private bool recording = false;

    public void Record()
    {
        transforms = new List<GhostTransform>();
        recording = true;
    }

    public void Stop()
    {
        recording = false;
    }

    public void Continue()
    {
        recording = true;
    }

    public void Save(string file)
    {
        XmlSerializer s = new XmlSerializer(typeof(GhostTransform[]));
        StreamWriter w = new StreamWriter(file);
        s.Serialize(w, transforms.ToArray());
        //foreach (GhostTransform t in transforms)
        //{
        //    s.Serialize(w, t);
        //}
        w.Close();

        //using (FileStream f = new FileStream(file, FileMode.Create))
        //{
        //    using (BinaryWriter b = new BinaryWriter(f))
        //    {
        //        foreach (Transform t in positions)
        //        {
        //            b.Write(t);
        //        }
        //    }
        //}




    }

    void FixedUpdate()
    {
        if (recording) {
            transforms.Add(new GhostTransform(transform));
        }
    }

    public float GetTime()
    {
        if (transforms != null)
        {
            return (transforms.Count * Time.fixedDeltaTime);
        }
        else
        {
            return 0;
        }
    }
}
