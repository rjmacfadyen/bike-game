﻿using UnityEngine;
using System.Collections;

public class GhostTransform
{
    public Vector3 position;
    public Quaternion rotation;

    public GhostTransform()
    {

    }

    public GhostTransform(Transform t)
    {
        position = t.position;
        rotation = t.rotation;
    }
}
