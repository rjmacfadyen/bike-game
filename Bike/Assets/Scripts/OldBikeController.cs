﻿using UnityEngine;
using System.Collections;

public class OldBikeController : MonoBehaviour {

    [SerializeField]
    private Transform normalTransform;
    private Rigidbody bike;

    [SerializeField]
    private Rigidbody steering;
    private HingeJoint steeringJoint;
    private JointSpring steeringSpring;
    [SerializeField]
    private Rigidbody drive;
    private HingeJoint driveJoint;
    private JointMotor driveMotor;

    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private float power = 10;
    [SerializeField]
    private float steeringLockAngle = 45;

    [SerializeField]
    private float lean = 1;
    [SerializeField]
    private float balance = 1;

    private bool onGround = false;

	// Use this for initialization
	void Start () {
        bike = GetComponent<Rigidbody>();

        steeringJoint = steering.GetComponent<HingeJoint>();
        steeringSpring = steeringJoint.spring;

        driveJoint = drive.GetComponent<HingeJoint>();
        driveMotor = driveJoint.motor;
        driveJoint.useMotor = true;

        driveMotor.force = power;
        driveJoint.motor = driveMotor;
	}
	
	// Update is called once per frame
	void Update () {
        onGround = !(Input.GetButton("Jump"));

        if (onGround)
        {
            // steering
            steeringSpring.targetPosition = Input.GetAxis("Horizontal") * steeringLockAngle;
            steeringJoint.spring = steeringSpring;

            // speed
            //bike.AddForce(Input.GetAxis("Vertical") * normalTransform.forward * speed * Time.deltaTime);

            driveMotor.targetVelocity = Input.GetAxis("Vertical") * speed;
            driveJoint.motor = driveMotor;
            Debug.Log(driveJoint.velocity / speed);

            // balancing
            // bike.rotation = Quaternion.Euler(bike.rotation.eulerAngles.x, bike.rotation.eulerAngles.y, Input.GetAxis("Horizontal") * -lean); // Quaternion.Lerp(bike.rotation, Quaternion.Euler(bike.rotation.eulerAngles.x, bike.rotation.eulerAngles.y, 0), balance);
            //bike.AddForceAtPosition(new Vector3(0, 0, Mathf.Sin(bike.angularVelocity.z * Mathf.Deg2Rad)), bike.transform.position + Vector3.up);
            
            bike.AddTorque(0, 0, -bike.mass * bike.angularVelocity.z, ForceMode.Force);
            
            //Vector3 av = bike.angularVelocity;
            //av.z = bike.rotation.eulerAngles.z;
            //bike.angularVelocity = av;


        }
        else
        {
            //bike.
        }
    }
}
