﻿using UnityEngine;
using System.Collections;

public class Rider : MonoBehaviour {

    private Vector3 localPos;
    private Quaternion localRot;

    private bool broken = false;

    public bool IsBroken { get { return broken; } }
	
	void Start () {
        localPos = transform.localPosition;
        localRot = transform.localRotation;
	}

    public void Break()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Collider>().enabled = true;
        broken = true;
    }

    public void Reset()
    {
        transform.localPosition = localPos;
        transform.localRotation = localRot;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().enabled = false;
        broken = false;
    }
}
