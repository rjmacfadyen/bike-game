﻿using UnityEngine;
using System.Collections;

public class RiderTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if (!GetComponentInParent<Rider>().IsBroken)
        {
            Debug.Log(col.gameObject.name);
            if (col.gameObject.CompareTag("Ground"))
            {
                GetComponentInParent<BikeBehaviour>().Crash(); ;
            }
        }
    }
}
