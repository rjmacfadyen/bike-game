﻿using UnityEngine;
using System.Collections;

public class SideCamera : MonoBehaviour {

    [SerializeField]
    private Transform target;
    [SerializeField]
    private float distance;
    [SerializeField]
    private float zoomScale;
    [SerializeField]
    [Range(0,1)]
    private float smoothing;

    private Vector3 lastTarget;

	// Use this for initialization
	void Start () {
        transform.rotation = Quaternion.Euler(0, 270, 0);
	}
	
	// Update is called once per frame
	void Update () {
        float speed = (target.position - lastTarget).z / Time.deltaTime;
        float zoom = Mathf.Abs(distance * (speed * zoomScale + 1));
        float lead = zoom * 0.5f;

        transform.position = Vector3.Lerp(transform.position, target.position + new Vector3(zoom, 0, lead), smoothing);
        lastTarget = target.position;
	}
}
