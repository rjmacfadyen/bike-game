﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI : MonoBehaviour {

    [SerializeField]
    private Text flash;
    private float flashTime = 1f;
    private float flashDuration = 2f;
    private float flashFadeIn = 0.15f;
    private float flashFadeOut = 0.75f;
    private float flashStartSize = 0.3f;
    private float flashEndSize = 1f;
    private Color flashColor = Color.white;
    private Queue<string> flashes = new Queue<string>();

    [SerializeField]
    private GameObject ending;

	// Update is called once per frame
	void Update () {
        FlashControl();
	}

    // flash controls

    private void FlashControl()
    {
        flashTime += Time.deltaTime / flashDuration;

        if (flashTime < 1)
        {
            flash.GetComponent<RectTransform>().localScale = Vector3.one * Mathf.Lerp(flashStartSize, flashEndSize, flashTime);
            flash.color = new Color(flashColor.r, flashColor.g, flashColor.b, Fade());
        }
        else
        {
            if (flash.enabled)
            {
                flash.enabled = false;
            }
            setFlash();
        }
    }

    public void ShowEnding(float time, bool win)
    {
        Debug.Log("showing ending");
        ending.SetActive(true);
        Debug.Log("shown UI");
        GameObject.Find("Time").GetComponent<Text>().text = System.String.Format("Time: {0:F2}", time);
        Debug.Log("shown time");
        GameObject.Find("Record").SetActive(win);
        Debug.Log("shown record");
    }

    public void Flash(string msg)
    {
        flashes.Enqueue(msg);
    }

    public void setFlash()
    {
        if (flashes.Count > 0)
        {
            string msg = flashes.Dequeue();
            flash.text = msg;
            flashTime = 0;
            flash.enabled = true;
        }
    }

    private float Fade()
    {
        float vis = 1f;

        if (flashTime < flashFadeIn)
        {
            vis = Mathf.Lerp(0f, 1f, flashTime / flashFadeIn);
        }
        else if (flashTime > flashFadeOut)
        {
            vis = Mathf.Lerp(1f, 0f, (flashTime - flashFadeOut) / (1f - flashFadeOut));
        }

        return vis;
    }
}

